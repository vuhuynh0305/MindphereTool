var express = require("express");
var bodyParser = require("body-parser");
var app = express();
var fetch = require("node-fetch");
var request = require("request");
var moment = require("moment");

app.use(bodyParser.json());

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, contentType,Content-Type, Accept, Authorization"
  );
  next();
});

// app.use(express.static(__dirname + "/dist"));

//PUT API
app.put("/api", function (req, res) {
  var machineId = req.body.machineID;
  var cameraId = req.body.cameraID;
  var option = req.body.option;
  var status = req.body.status;
  var parameter = req.body.parameter;
  var startTime = new Date(req.body.from);
  var token = req.headers.authorization;

  var variables = [];
  var inputData = [];

  for (var i = 0; i < status.length; i++) {
    var value = false;
    if (status[i].value === "1") {
      value = true;
    } else if (status[i].value === "0") {
      value = false;
    } else value = status[i].value;

    variables.push({
      name: status[i].name,
      value: value,
      qualityCode: "0"
    });
  }
  if (option == 1) {
    inputData.push({
      time: startTime,
      variables: variables
    });
  } else {
    var endTime = new Date(req.body.to);

    startTime.setSeconds(0);
    endTime.setSeconds(0);
    var endTime = new Date(req.body.to);
    var diff = substractTime(endTime, startTime);
    var num_of_data_points = diff * 60 / parameter;
    for (var i = 0; i < num_of_data_points; i++) {
      var time = new Date(startTime);
      time.setSeconds(time.getSeconds() + parameter * i);
      inputData.push({
        time: time,
        variables: variables
      });
    }
  }
  
  var url =
    "https://mindgate.appsdev.mindsphere.io/api/timeseries/api/assets/" +
    machineId +
    "/aspects/" +
    cameraId +
    "/timeseries";
  request.put(
    {
      headers: {
        "Content-type": "application/json",
        Authorization: token
      },
      url: url,
      body: JSON.stringify(inputData)
    },
    (error, response, body) => {
      if (error) {
        return console.dir(error);
      }
      res.send(body);
    }
  );
  // console.log(inputData);
  // res.send(inputData);
});

app.get("/getTimeseries", function (req, res) {
  var token = req.headers.authorization;

  request.get({
      headers: {
        "Content-type": "application/json",
        Authorization: token
      },
      url: 'https://mindgate.appsdev.mindsphere.io/api/timeseries/api/assets/8DAF6E6FA00248AE84541B633F26D74F/aspects/FEDD8A81B81C4043811A2949A8037E/timeseries?sort=time,asc&filter={ "time" : { "ge": { "value" : "2017-01-01T00:00:00.001Z" }, "le": { "value" : "2017-01-02T00:00:00.001Z" } } }'
    },
    function (error, response, body) {
      if (error) {
        return console.dir(error);
      }
      res.send(JSON.parse(body));
    }
  );
});

function substractTime(toTime, fromTime) {
  var to = toTime.getHours() * 60 + toTime.getMinutes();
  var from = fromTime.getHours() * 60 + fromTime.getMinutes();
  var diffTime = to - from;
  return diffTime;
}

app.listen(3000, function () {
  console.log("App is listening on port 3000!");
});
