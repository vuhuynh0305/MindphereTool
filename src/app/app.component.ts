import { Component, ViewChild, ElementRef, ViewContainerRef } from '@angular/core';
import { HttpClient, HttpHeaders, } from '@angular/common/http';
import { Headers, Http } from '@angular/http';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import * as moment from 'moment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  token = '';
  headers: Headers;
  user = { id: 1, name: 'Hello' };
  @ViewChild('btn') btn: ElementRef;
  loop: any;
  statuses: IStatus[] = [
    {
      name: '',
      value: null
    }
  ];

  startDate = new Date().setHours(0, 0, 0, 0);

  data: any = {
    machineID: null,
    cameraID: null,
    option: 1,
    day: new Date(this.startDate),
    from: new Date(this.startDate),
    to: new Date(this.startDate),
    status: this.statuses,
    parameter: null
  };

  endDate = new Date(this.data.day).setHours(23, 59, 59, 999);

  constructor(
    private http: Http,
    private toastr: ToastsManager,
    private vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  requestHeaders() {
    this.headers = new Headers();
    this.headers.append('Content-Type', `application/json`);
    this.headers.append('Authorization', `Bearer ${this.token}`);
  }

  callPutServer(url: string, data) {
    this.requestHeaders();
    const path = 'http://localhost:3000/' + url;
    // tslint:disable-next-line:no-shadowed-variable
    this.http.put(path, JSON.stringify(data), { headers: this.headers }).subscribe((data: any) => {
      if (data._body !== "") {
        this.toastr.error("There is an error! Please check all fields again.", null, { toastLife: 3000 });
      }
      else {
        this.toastr.success("Push successfully.", null, { toastLife: 3000 });
      }
    });
  }

  getTimeSeries() {
    this.requestHeaders();
    this.http.get('http://localhost:3000/getTimeseries', { headers: this.headers }).subscribe((data: any) => {
      
    });
  }

  addStatus() {
    const status: IStatus = {
      name: null,
      value: null
    };
    this.statuses.push(status);
  }

  removeOneStatus(i: number) {
    this.statuses.splice(i, 1);
  }

  detectSelectedDate(date: Date) {
    this.data.from = new Date(date);
    this.data.to = new Date(date);
    this.endDate = new Date(date).setHours(23, 59, 59, 999);
  }

  submitForm() {
    const submitData = Object.assign({}, this.data);
    if (this.data.option == 1) {
      if (this.btn.nativeElement.innerText == 'Start') {
        this.btn.nativeElement.innerText = 'Stop';
        const currentDate = new Date();
        const parameter = this.data.parameter;
        let count = 0;
        this.loop = setInterval(() => {
          let time = moment(currentDate).utcOffset(0).second(parameter * count);
          submitData.from = time.format();
          this.callPutServer('api', submitData);
          this.toastr.info("Sending at time " + time.format("DD-MM-YYYY HH:mm:ss"), null, { toastLife: 3000 });
          count++;
        }, parameter * 1000);

      }
      else {
        this.btn.nativeElement.innerText = 'Start';
        clearInterval(this.loop);
      }
    }
    else {
      const date = moment(submitData.day);
      submitData.from = moment(this.data.from).utcOffset('+09:00', true).utcOffset(0).format();
      submitData.to = moment(this.data.to).utcOffset('+09:00', true).utcOffset(0).format();
      this.callPutServer('api', submitData);
    }
  }
}

interface IStatus {
  name: string;
  value: any;
}
