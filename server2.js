var formidable = require("formidable");
var http = require("http");
var fs = require("fs");
var parseCsv = require('csv-array');
var request = require("request");
var resSrv = null;

const server = http.createServer(function (req, res) {
  resSrv = res;
  if (req.url == "/fileupload") {
    var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
      parseData(files).then(processData).then(pushData);
    })
  } else {
    res.writeHead(200, {
      "Content-Type": "text/html"
    });
    res.write(
      '<form action="fileupload" method="post" enctype="multipart/form-data">'
    );
    res.write('<input type="file" name="fileConfig"><br>');
    res.write('<input type="file" name="fileData"><br>');
    res.write('<input type="submit">');
    res.write("</form>");
    res.end();
  }
});

server.listen(8080, 'localhost', function(){
  console.log('App is listening on port 8080!');
});

async function parseData(files) {
  config = await readConfigFile(files.fileConfig.path);
  content = await readContentFile(files.fileData.path);
}

function readConfigFile(path) {
  return new Promise(function (resolve, reject) {
    fs.readFile(path, 'utf8', function (err, data) {
      if (err) reject(err);
      resolve(JSON.parse(data));
    });
  });
};

function readContentFile(path) {
  return new Promise(function (resolve, reject) {
    parseCsv.parseCSV(path, function (data) {
      resolve(JSON.stringify(data));
    });
  });
}

function processData() {
  machineId = config.machineId;
  cameraId = config.cameraId;
  token = config.token;
  var variables = [];
  inputData = [];
  var contentData = JSON.parse(content);
  var prop = [];

  for (var key in contentData[0]) {
    prop.push(key);
  }

  for (var i = 0; i < contentData.length; i++) {
    var time;
    for (var j = 0; j < prop.length; j++) {
      if (prop[j] != 'time') {
        variables.push({
          name: prop[j],
          value: contentData[i][prop[j]],
          qualityCode: "0"
        })
      } else {
        time = contentData[i][prop[j]];
      }
    }
    inputData.push({
      time: time,
      variables: variables
    });
  }
}

function pushData() {
  var url = "https://mindgate.appsdev.mindsphere.io/api/timeseries/api/assets/" + machineId + "/aspects/" + cameraId + "/timeseries";
  request.put({
    headers: {
      "Content-type": "application/json",
      'Authorization': token
    },
    url: url,
    body: JSON.stringify(inputData)
  }, function (error, response, body) {
    if (error) {
      return console.dir(error);
    }
    resSrv.write(body);
    resSrv.end();
  });
}
